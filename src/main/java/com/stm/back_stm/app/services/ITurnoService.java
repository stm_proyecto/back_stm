package com.stm.back_stm.app.services;

import java.util.List;
import java.util.Optional;

import com.stm.back_stm.app.domain.entity.Turno;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.TurnoDTO;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

public interface ITurnoService {
	
	/** Consultar todas los Turnos */
	List<Turno> consultarTurno();
	
	/** Guardar Turno */
	TurnoDTO guardarTurno(TurnoDTO turnoDTO) throws ServiceException;
	
	/** Actualizar Turno */
	TurnoDTO actualizarTurno(TurnoDTO turnoDTO) throws ServiceException;
	
	/** Consultar Turno por ID */
	Optional<Turno> consultarTurnoId(Long Id);
	
	/** Elimina Turno por ID */
	void eliminarTurno(Long id);
	
}
