package com.stm.back_stm.app.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stm.back_stm.app.domain.entity.EmpleadoUSAE;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.EmpleadoUSAEDTO;
import com.stm.back_stm.app.repository.IEmpleadoUSAERepository;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Service
public class EmpleadoUSAEService implements IEmpleadoUSAEService{

	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadoUSAEService.class);
	
	@Autowired
	private IEmpleadoUSAERepository iEmpleadoUSAERepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	/** Consulta todas */
	@Override
	public List<EmpleadoUSAE> consultarEmpleadoUSAE() {
		
		LOGGER.info("Entra a service para consultar");

		List<EmpleadoUSAE> lista = new ArrayList<EmpleadoUSAE>();

		Iterable<EmpleadoUSAE> parametro = iEmpleadoUSAERepository.findAll();
		parametro.forEach(lista::add);

		return lista;
		
	}
	
	/** Guardar */
	@Override
	public EmpleadoUSAEDTO guardarEmpleadoUSAE(EmpleadoUSAEDTO empleadoUSAEDTO) throws ServiceException {
		
		LOGGER.info("Entra a service para guardar EmpleadoUSAE");
		
    	EmpleadoUSAE oEmpleadoUSAE = null;
    	
		try {
			oEmpleadoUSAE = convertToEntity(empleadoUSAEDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oEmpleadoUSAE.toString());
		 
		EmpleadoUSAE empleadoUSAEN = iEmpleadoUSAERepository.save(oEmpleadoUSAE);
		
		EmpleadoUSAEDTO regresa = convertToDto(empleadoUSAEN);
		
		return regresa;			
	}
	
	
	
	/** Actualizar */
	@Override
	public EmpleadoUSAEDTO actualizarEmpleadoUSAE(EmpleadoUSAEDTO empleadoUSAEDTO) throws ServiceException {
		
		EmpleadoUSAE oEmpleadoUSAE = null;
		try {
			oEmpleadoUSAE = convertToEntity(empleadoUSAEDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oEmpleadoUSAE.toString());
		
		EmpleadoUSAE empleadoUSAEN = iEmpleadoUSAERepository.save(oEmpleadoUSAE);
		
		EmpleadoUSAEDTO regresa = convertToDto(empleadoUSAEN);
		
		return regresa;
	}

	
	/** Buscar por id*/
	@Override
	public Optional<EmpleadoUSAE> consultarEmpleadoUSAEId(Long id) {
		Optional<EmpleadoUSAE> empleadoUSAE = null;
		try {

			if (empleadoUSAE == null) {
				empleadoUSAE = iEmpleadoUSAERepository.findById(id);
			}
			
			if (empleadoUSAE == null) {
				throw new ServiceException("empleadoUSAE no encontrado");
			}

		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
		}
		
		return empleadoUSAE;
	}
	
	/** Eliminar*/
	@Override
	public void eliminarEmpleadoUSAE(Long id) {
		iEmpleadoUSAERepository.deleteById(id);
		
	}
	
	private EmpleadoUSAE convertToEntity(EmpleadoUSAEDTO pEmpleadoUSAEDTO) throws ParseException {
		EmpleadoUSAE regresa = modelMapper.map(pEmpleadoUSAEDTO, EmpleadoUSAE.class);
	    return regresa;
	}
	
	private EmpleadoUSAEDTO convertToDto(EmpleadoUSAE pEmpleadoUSAEDTO) {
		EmpleadoUSAEDTO regresa = modelMapper.map(pEmpleadoUSAEDTO, EmpleadoUSAEDTO.class);
	    return regresa;
	}

}
