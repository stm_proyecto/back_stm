package com.stm.back_stm.app.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stm.back_stm.app.domain.entity.Servicio;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.ServicioDTO;
import com.stm.back_stm.app.repository.IServicioRepository;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Service
public class ServicioService implements IServicioService {

	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(ServicioService.class);
	
	@Autowired
	private IServicioRepository iServicioRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	/** Consulta todas */
	@Override
	public List<Servicio> consultarServicio() {
		
		LOGGER.info("Entra a service para consultar");

		List<Servicio> lista = new ArrayList<Servicio>();

		Iterable<Servicio> parametro = iServicioRepository.findAll();
		parametro.forEach(lista::add);

		return lista;
		
	}
	
	/** Guardar */
	@Override
	public ServicioDTO guardarServicio(ServicioDTO servicioDTO) throws ServiceException {
		
		LOGGER.info("Entra a service para guardar Servicio");
		
    	Servicio oServicio = null;
    	
		try {
			oServicio = convertToEntity(servicioDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oServicio.toString());
		 
		Servicio servicioN = iServicioRepository.save(oServicio);
		
		ServicioDTO regresa = convertToDto(servicioN);
		
		return regresa;			
	}
	
	
	
	/** Actualizar */
	@Override
	public ServicioDTO actualizarServicio(ServicioDTO servicioDTO) throws ServiceException {
		
		Servicio oServicio = null;
		try {
			oServicio = convertToEntity(servicioDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oServicio.toString());
		
		Servicio servicioN = iServicioRepository.save(oServicio);
		
		ServicioDTO regresa = convertToDto(servicioN);
		
		return regresa;
	}

	
	/** Buscar por id*/
	@Override
	public Optional<Servicio> consultarServicioId(Long id) {
		Optional<Servicio> servicio = null;
		try {

			if (servicio == null) {
				servicio = iServicioRepository.findById(id);
			}
			
			if (servicio == null) {
				throw new ServiceException("servicio no encontrado");
			}

		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
		}
		
		return servicio;
	}
	
	/** Eliminar*/
	@Override
	public void eliminarServicio(Long id) {
		iServicioRepository.deleteById(id);
		
	}
	
	private Servicio convertToEntity(ServicioDTO pServicioDTO) throws ParseException {
		Servicio regresa = modelMapper.map(pServicioDTO, Servicio.class);
	    return regresa;
	}
	
	private ServicioDTO convertToDto(Servicio pServicioDTO) {
		ServicioDTO regresa = modelMapper.map(pServicioDTO, ServicioDTO.class);
	    return regresa;
	}

}
