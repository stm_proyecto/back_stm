package com.stm.back_stm.app.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stm.back_stm.app.domain.entity.AreaServicio;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.AreaServicioDTO;
import com.stm.back_stm.app.repository.IAreaServicioRepository;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Service
public class AreaServicioService implements IAreaServicioService{
	
	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(AreaServicioService.class);
	
	@Autowired
	private IAreaServicioRepository iAreaServicioRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	/** Consulta todas */
	@Override
	public List<AreaServicio> consultarAreaServicio() {
		
		LOGGER.info("Entra a service para consultar");

		List<AreaServicio> lista = new ArrayList<AreaServicio>();

		Iterable<AreaServicio> parametro = iAreaServicioRepository.findAll();
		parametro.forEach(lista::add);

		return lista;
		
	}
	
	/** Guardar */
	@Override
	public AreaServicioDTO guardarAreaServicio(AreaServicioDTO areaServicioDTO) throws ServiceException {
		
		LOGGER.info("Entra a service para guardar AreaServicio");
		
    	AreaServicio oAreaServicio = null;
    	
		try {
			oAreaServicio = convertToEntity(areaServicioDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oAreaServicio.toString());
		 
		AreaServicio areaServicioN = iAreaServicioRepository.save(oAreaServicio);
		
		AreaServicioDTO regresa = convertToDto(areaServicioN);
		
		return regresa;			
	}
	
	
	
	/** Actualizar */
	@Override
	public AreaServicioDTO actualizarAreaServicio(AreaServicioDTO areaServicioDTO) throws ServiceException {
		
		AreaServicio oAreaServicio = null;
		try {
			oAreaServicio = convertToEntity(areaServicioDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oAreaServicio.toString());
		
		AreaServicio areaServicioN = iAreaServicioRepository.save(oAreaServicio);
		
		AreaServicioDTO regresa = convertToDto(areaServicioN);
		
		return regresa;
	}

	
	/** Buscar por id*/
	@Override
	public Optional<AreaServicio> consultarAreaServicioId(Long id) {
		Optional<AreaServicio> areaServicio = null;
		try {

			if (areaServicio == null) {
				areaServicio = iAreaServicioRepository.findById(id);
			}
			
			if (areaServicio == null) {
				throw new ServiceException("areaServicio no encontrada");
			}

		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
		}
		
		return areaServicio;
	}
	
	/** Eliminar*/
	@Override
	public void eliminarAreaServicio(Long id) {
		iAreaServicioRepository.deleteById(id);
		
	}
	
	private AreaServicio convertToEntity(AreaServicioDTO pAreaServicioDTO) throws ParseException {
		AreaServicio regresa = modelMapper.map(pAreaServicioDTO, AreaServicio.class);
	    return regresa;
	}
	
	private AreaServicioDTO convertToDto(AreaServicio pAreaServicio) {
		AreaServicioDTO regresa = modelMapper.map(pAreaServicio, AreaServicioDTO.class);
	    return regresa;
	}

	
}
