package com.stm.back_stm.app.services;

import java.util.List;
import java.util.Optional;

import com.stm.back_stm.app.domain.entity.AreaServicio;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.AreaServicioDTO;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

public interface IAreaServicioService {
	
	/** Consultar todas las areas de servicio */
	List<AreaServicio> consultarAreaServicio();
	
	/** Guardar AreaServicio */
	AreaServicioDTO guardarAreaServicio(AreaServicioDTO areaServicioDTO) throws ServiceException;
	
	/** Actualizar AreaServicio */
	AreaServicioDTO actualizarAreaServicio(AreaServicioDTO areaServicioDTO) throws ServiceException;
	
	/** Consultar AreaServicio por ID */
	Optional<AreaServicio> consultarAreaServicioId(Long Id);
	
	/** Elimina AreaServicio por ID */
	void eliminarAreaServicio(Long id);

	
}
