package com.stm.back_stm.app.services;

import java.util.List;
import java.util.Optional;

import com.stm.back_stm.app.domain.entity.EmpleadoUSAE;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.EmpleadoUSAEDTO;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

public interface IEmpleadoUSAEService {
	
	/** Consultar todas los empleados USAE */
	List<EmpleadoUSAE> consultarEmpleadoUSAE();
	
	/** Guardar EmpleadoUSAE */
	EmpleadoUSAEDTO guardarEmpleadoUSAE(EmpleadoUSAEDTO empleadoUSAEDTO) throws ServiceException;
	
	/** Actualizar EmpleadoUSAE */
	EmpleadoUSAEDTO actualizarEmpleadoUSAE(EmpleadoUSAEDTO empleadoUSAEDTO) throws ServiceException;
	
	/** Consultar EmpleadoUSAE por ID */
	Optional<EmpleadoUSAE> consultarEmpleadoUSAEId(Long Id);
	
	/** Elimina EmpleadoUSAE por ID */
	void eliminarEmpleadoUSAE(Long id);

}
