package com.stm.back_stm.app.services;

import java.util.List;
import java.util.Optional;

import com.stm.back_stm.app.domain.entity.Servicio;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.ServicioDTO;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

public interface IServicioService {
	
	/** Consultar todas los servicios */
	List<Servicio> consultarServicio();
	
	/** Guardar Servicio */
	ServicioDTO guardarServicio(ServicioDTO servicioDTO) throws ServiceException;
	
	/** Actualizar Servicio */
	ServicioDTO actualizarServicio(ServicioDTO servicioDTO) throws ServiceException;
	
	/** Consultar Servicio por ID */
	Optional<Servicio> consultarServicioId(Long Id);
	
	/** Elimina Servicio por ID */
	void eliminarServicio(Long id);

}
