package com.stm.back_stm.app.services;

import java.util.List;
import java.util.Optional;

import com.stm.back_stm.app.domain.entity.Empleado;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.EmpleadoDTO;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

public interface IEmpleadoService {
	
	/** Consultar todas los empleados */
	List<Empleado> consultarEmpleado();
	
	/** Guardar Empleado */
	EmpleadoDTO guardarEmpleado(EmpleadoDTO empleadoDTO) throws ServiceException;
	
	/** Actualizar Empleado */
	EmpleadoDTO actualizarEmpleado(EmpleadoDTO empleadoDTO) throws ServiceException;
	
	/** Consultar Empleado por ID */
	Optional<Empleado> consultarEmpleadoId(Long Id);
	
	/** Elimina Empleado por ID */
	void eliminarEmpleado(Long id);
	
}
