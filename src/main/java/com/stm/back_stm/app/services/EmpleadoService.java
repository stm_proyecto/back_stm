package com.stm.back_stm.app.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stm.back_stm.app.domain.entity.Empleado;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.EmpleadoDTO;
import com.stm.back_stm.app.repository.IEmpleadoRepository;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Service
public class EmpleadoService implements IEmpleadoService{

	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadoService.class);
	
	@Autowired
	private IEmpleadoRepository iEmpleadoRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	/** Consulta todas */
	@Override
	public List<Empleado> consultarEmpleado() {
		
		LOGGER.info("Entra a service para consultar");

		List<Empleado> lista = new ArrayList<Empleado>();

		Iterable<Empleado> parametro = iEmpleadoRepository.findAll();
		parametro.forEach(lista::add);

		return lista;
		
	}
	
	/** Guardar */
	@Override
	public EmpleadoDTO guardarEmpleado(EmpleadoDTO empleadoDTO) throws ServiceException {
		
		LOGGER.info("Entra a service para guardar Empleado");
		
    	Empleado oEmpleado = null;
    	
		try {
			oEmpleado = convertToEntity(empleadoDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oEmpleado.toString());
		 
		Empleado empleadoN = iEmpleadoRepository.save(oEmpleado);
		
		EmpleadoDTO regresa = convertToDto(empleadoN);
		
		return regresa;			
	}
	
	
	
	/** Actualizar */
	@Override
	public EmpleadoDTO actualizarEmpleado(EmpleadoDTO empleadoDTO) throws ServiceException {
		
		Empleado oEmpleado = null;
		try {
			oEmpleado = convertToEntity(empleadoDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oEmpleado.toString());
		
		Empleado empleadoN = iEmpleadoRepository.save(oEmpleado);
		
		EmpleadoDTO regresa = convertToDto(empleadoN);
		
		return regresa;
	}

	
	/** Buscar por id*/
	@Override
	public Optional<Empleado> consultarEmpleadoId(Long id) {
		Optional<Empleado> empleado = null;
		try {

			if (empleado == null) {
				empleado = iEmpleadoRepository.findById(id);
			}
			
			if (empleado == null) {
				throw new ServiceException("empleado no encontrado");
			}

		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
		}
		
		return empleado;
	}
	
	/** Eliminar*/
	@Override
	public void eliminarEmpleado(Long id) {
		iEmpleadoRepository.deleteById(id);
		
	}
	
	private Empleado convertToEntity(EmpleadoDTO pEmpleadoDTO) throws ParseException {
		Empleado regresa = modelMapper.map(pEmpleadoDTO, Empleado.class);
	    return regresa;
	}
	
	private EmpleadoDTO convertToDto(Empleado pEmpleadoDTO) {
		EmpleadoDTO regresa = modelMapper.map(pEmpleadoDTO, EmpleadoDTO.class);
	    return regresa;
	}

}
