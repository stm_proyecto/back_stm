package com.stm.back_stm.app.services;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stm.back_stm.app.domain.entity.Turno;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.TurnoDTO; 
import com.stm.back_stm.app.repository.ITurnoRepository;

@Service
public class TurnoService implements ITurnoService{
	
	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(TurnoService.class);
	
	@Autowired
	private ITurnoRepository iTurnoRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	/** Consulta todas */
	@Override
	public List<Turno> consultarTurno() {
		
		LOGGER.info("Entra a service para consultar");

		List<Turno> lista = new ArrayList<Turno>();

		Iterable<Turno> parametro = iTurnoRepository.findAll();
		parametro.forEach(lista::add);

		return lista;
		
	}
	
	/** Guardar */
	@Override
	public TurnoDTO guardarTurno(TurnoDTO turnoDTO) throws ServiceException {
		
		LOGGER.info("Entra a service para guardar Turno");
		
    	Turno oTurno = null;
    	
		try {
			oTurno = convertToEntity(turnoDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oTurno.toString());
		 
		Turno turnoN = iTurnoRepository.save(oTurno);
		
		TurnoDTO regresa = convertToDto(turnoN);
		
		return regresa;			
	}
	
	
	
	/** Actualizar */
	@Override
	public TurnoDTO actualizarTurno(TurnoDTO turnoDTO) throws ServiceException {
		
		Turno oTurno = null;
		try {
			oTurno = convertToEntity(turnoDTO);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorEnum.EXC_ERROR_PARAMS);
		}
		LOGGER.debug(oTurno.toString());
		
		Turno turnoN = iTurnoRepository.save(oTurno);
		
		TurnoDTO regresa = convertToDto(turnoN);
		
		return regresa;
	}

	
	/** Buscar por id*/
	@Override
	public Optional<Turno> consultarTurnoId(Long id) {
		Optional<Turno> turno = null;
		try {

			if (turno == null) {
				turno = iTurnoRepository.findById(id);
			}
			
			if (turno == null) {
				throw new ServiceException("turno no encontrado");
			}

		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
		}
		
		return turno;
	}
	
	/** Eliminar*/
	@Override
	public void eliminarTurno(Long id) {
		iTurnoRepository.deleteById(id);
		
	}
	
	private Turno convertToEntity(TurnoDTO pTurnoDTO) throws ParseException {
		Turno regresa = modelMapper.map(pTurnoDTO, Turno.class);
	    return regresa;
	}
	
	private TurnoDTO convertToDto(Turno pTurnoDTO) {
		TurnoDTO regresa = modelMapper.map(pTurnoDTO, TurnoDTO.class);
	    return regresa;
	}

}
