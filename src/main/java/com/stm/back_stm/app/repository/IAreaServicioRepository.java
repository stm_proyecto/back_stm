package com.stm.back_stm.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.stm.back_stm.app.domain.entity.AreaServicio;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Repository
public interface IAreaServicioRepository extends CrudRepository<AreaServicio, Long> {

}
