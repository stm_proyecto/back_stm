package com.stm.back_stm.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.stm.back_stm.app.domain.entity.EmpleadoUSAE;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Repository
public interface IEmpleadoUSAERepository extends CrudRepository<EmpleadoUSAE, Long> {

}
