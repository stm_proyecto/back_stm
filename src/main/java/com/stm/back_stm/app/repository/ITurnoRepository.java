package com.stm.back_stm.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.stm.back_stm.app.domain.entity.Turno;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Repository
public interface ITurnoRepository extends CrudRepository<Turno, Long> {

}
