package com.stm.back_stm.app.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Getter
@Setter
@Entity
@ToString
@Table(name = "turno")
public class Turno implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "folio")
	private Long folio;

	@Column(name = "descripcion")
	private String descripcion;
	
	@ManyToOne
	@JoinColumn(name="id_servicio")
	private Servicio idServicio;
	
	@ManyToOne
	@JoinColumn(name="id_empleado")
	private Empleado IdEmpleado;
	
	@ManyToOne
	@JoinColumn(name="id_area")
	private AreaServicio idArea;
	
}
