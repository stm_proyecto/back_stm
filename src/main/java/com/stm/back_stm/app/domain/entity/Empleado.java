package com.stm.back_stm.app.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Getter
@Setter
@Entity
@ToString
@Table(name = "empleado")
public class Empleado implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_empleado")
	private Long idEmpleado;
	
	@Column(name = "cve")
	private String cve;

	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "app")
	private String app;
	
	@Column(name = "apm")
	private String apm;
	
}
