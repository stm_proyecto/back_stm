package com.stm.back_stm.app.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Getter
@Setter
@ToString
public class TurnoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long folio;

	private String descripcion;
	
	private ServicioDTO idServicio;
	
	private EmpleadoDTO idEmpleado;
	
	private AreaServicioDTO idArea;
	
}
