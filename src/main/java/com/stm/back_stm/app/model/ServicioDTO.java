package com.stm.back_stm.app.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Getter
@Setter
@ToString
public class ServicioDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long idServicio;
	
	private String nombreServicio;
	
	private AreaServicioDTO idArea;
	
}
