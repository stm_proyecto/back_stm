package com.stm.back_stm.app.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@Getter
@Setter
@ToString
public class EmpleadoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long idEmpleado;
	
	private String cve;

	private String nombre;
	
	private String app;
	
	private String apm;

}
