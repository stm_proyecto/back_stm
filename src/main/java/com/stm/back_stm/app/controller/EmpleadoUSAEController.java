package com.stm.back_stm.app.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stm.back_stm.app.domain.entity.EmpleadoUSAE;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.EmpleadoUSAEDTO;
import com.stm.back_stm.app.services.IEmpleadoUSAEService;
import com.stm.back_stm.util.Constantes;
import com.stm.back_stm.util.Respuesta;

import io.swagger.annotations.ApiOperation;


/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@RestController
@CrossOrigin
@RequestMapping("/stm/empleados-usae")
public class EmpleadoUSAEController {


	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadoUSAEController.class);	
	
	
	@Autowired
	IEmpleadoUSAEService iEmpleadoUSAEService;
	
	/** Servicio para consultar todo */
	@ApiOperation(value = "Obtiene todos los registros de empleadoUSAE", notes = "Devuelve un listado de todos los registros de empleadoUSAE.")
	@GetMapping
	public ResponseEntity<Respuesta> consultaEmpleadoUSAE() {
		
		LOGGER.info("Entra a controller para consultar las empleadoUSAE");
		
		List<EmpleadoUSAE> lista;
		Respuesta response = new Respuesta();		
		
		lista = iEmpleadoUSAEService.consultarEmpleadoUSAE();
		
		if (lista.isEmpty()) {
			LOGGER.warn("Sin Datos en la base de datos");
			
			response.setEstatus("OK");
			response.setMensaje("Consulta exitosa - sin datos en la base de datos");
			response.setLista(lista);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		LOGGER.warn("Consulta Exitosa");
		
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setLista(lista);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para guardar 
	 * @throws ParseException */
	@ApiOperation(value = "Inserta nuevo registro de empleadoUSAE", notes = "Devuelve el registro nuevo del empleadoUSAE insertado.")
    @PostMapping
	public ResponseEntity<Respuesta> guardarEmpleadoUSAE(@RequestBody EmpleadoUSAEDTO empleadoUSAEDTO) throws ServiceException {
		
		LOGGER.info("Entra a controller para guardar las materias");
		LOGGER.info(Constantes.MENSAJE_LOG_PARAMETROS, empleadoUSAEDTO);
		
		Respuesta response = new Respuesta();

		empleadoUSAEDTO = iEmpleadoUSAEService.guardarEmpleadoUSAE(empleadoUSAEDTO);
		
		response.setEstatus("OK");
		response.setMensaje("Guardado exitoso");	
		response.setValor(empleadoUSAEDTO.getIdEmpleadoUSAE().toString());
		response.setDto(empleadoUSAEDTO);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/** Servicio para actualizar 
	 * @throws ServiceException */
	@ApiOperation(value = "Actualiza el registro del empleadoUSAE", notes = "Devuelve el registro del empleadoUSAE del id especificado.")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Respuesta> actualizarEmpleadoUSAE(
			@PathVariable("id") Long id,
	        @Valid @RequestBody EmpleadoUSAEDTO empleadoUSAEDTO) throws ServiceException {

		LOGGER.info("Entra a controller, actualizacion : {}", empleadoUSAEDTO);
		Respuesta response = new Respuesta();
		
	    	
		empleadoUSAEDTO.setIdEmpleadoUSAE(id);
		empleadoUSAEDTO = iEmpleadoUSAEService.actualizarEmpleadoUSAE(empleadoUSAEDTO);
		response.setEstatus("OK");
		response.setMensaje("Actualizacion exitoso");
		response.setValor(empleadoUSAEDTO.getIdEmpleadoUSAE().toString());
		response.setDto(empleadoUSAEDTO);
			
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para consultar por id 
	 * @throws ServiceException */
	@ApiOperation(value = "Buscar empleadoUSAE por Id", notes = "Devuelve el empleadoUSAE en base al id especificado.")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Respuesta> consultarEmpleadoUSAEPorId(@PathVariable("id") Long id) throws ServiceException {
		
		LOGGER.info("Entra controller Aduana por id {}", id);

		Optional<EmpleadoUSAE> empleadoUSAE;
		Respuesta response = new Respuesta();

		if (id == null) {
			LOGGER.warn("Se necesita parametro Id");
			response.setEstatus("error");
			response.setMensaje("Se necesita parametro Id");
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		empleadoUSAE = iEmpleadoUSAEService.consultarEmpleadoUSAEId(id);
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setDto(empleadoUSAE);
		if (!empleadoUSAE.isPresent()) {
			LOGGER.info("Registro no encontrado");
			throw new ServiceException(ErrorEnum.EXC_INEXISTENTE);
		} else {
			response.setValor(empleadoUSAE.get().getIdEmpleadoUSAE().toString());
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK); 
	}
	
	/** Servicio para borrar */
	@ApiOperation(value = "Elimina el registro del empleadoUSAE", notes = "No devuelve ningun valor.")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Respuesta> eliminarEmpleadoUSAE(
			@PathVariable("id") Long id) {

		LOGGER.info("Entra a controller, eliminacion de empleadoUSAE id: {}", id);
		Respuesta response = new Respuesta();

		iEmpleadoUSAEService.eliminarEmpleadoUSAE(id);
		response.setEstatus("OK");
		response.setMensaje("Borrado exitosa");
		response.setValor(id.toString());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}	

}
