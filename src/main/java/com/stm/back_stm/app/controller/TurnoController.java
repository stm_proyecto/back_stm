package com.stm.back_stm.app.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stm.back_stm.app.domain.entity.Turno;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.TurnoDTO;
import com.stm.back_stm.app.services.ITurnoService;
import com.stm.back_stm.util.Constantes;
import com.stm.back_stm.util.Respuesta;

import io.swagger.annotations.ApiOperation;


/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@RestController
@CrossOrigin
@RequestMapping("/stm/turnos")
public class TurnoController {


	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(TurnoController.class);	
	
	
	@Autowired
	ITurnoService iTurnoService;
	
	/** Servicio para consultar todo */
	@ApiOperation(value = "Obtiene todos los registros de turno", notes = "Devuelve un listado de todos los registros de turno.")
	@GetMapping
	public ResponseEntity<Respuesta> consultaTurno() {
		
		LOGGER.info("Entra a controller para consultar las turno");
		
		List<Turno> lista;
		Respuesta response = new Respuesta();		
		
		lista = iTurnoService.consultarTurno();
		
		if (lista.isEmpty()) {
			LOGGER.warn("Sin Datos en la base de datos");
			
			response.setEstatus("OK");
			response.setMensaje("Consulta exitosa - sin datos en la base de datos");
			response.setLista(lista);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		LOGGER.warn("Consulta Exitosa");
		
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setLista(lista);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para guardar 
	 * @throws ParseException */
	@ApiOperation(value = "Inserta nuevo registro de turno", notes = "Devuelve el registro nuevo del turno insertado.")
    @PostMapping
	public ResponseEntity<Respuesta> guardarTurno(@RequestBody TurnoDTO turnoDTO) throws ServiceException {
		
		LOGGER.info("Entra a controller para guardar los turnos");
		LOGGER.info(Constantes.MENSAJE_LOG_PARAMETROS, turnoDTO);
		
		Respuesta response = new Respuesta();

		turnoDTO = iTurnoService.guardarTurno(turnoDTO);
		
		response.setEstatus("OK");
		response.setMensaje("Guardado exitoso");	
		response.setValor(turnoDTO.getFolio().toString());
		response.setDto(turnoDTO);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/** Servicio para actualizar 
	 * @throws ServiceException */
	@ApiOperation(value = "Actualiza el registro del turno", notes = "Devuelve el registro del turno del id especificado.")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Respuesta> actualizarTurno(
			@PathVariable("id") Long id,
	        @Valid @RequestBody TurnoDTO turnoDTO) throws ServiceException {

		LOGGER.info("Entra a controller, actualizacion : {}", turnoDTO);
		Respuesta response = new Respuesta();
		
	    	
		turnoDTO.setFolio(id);
		turnoDTO = iTurnoService.actualizarTurno(turnoDTO);
		response.setEstatus("OK");
		response.setMensaje("Actualizacion exitoso");
		response.setValor(turnoDTO.getFolio().toString());
		response.setDto(turnoDTO);
			
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para consultar por id 
	 * @throws ServiceException */
	@ApiOperation(value = "Buscar turno por Id", notes = "Devuelve el turno en base al id especificado.")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Respuesta> consultarTurnoPorId(@PathVariable("id") Long id) throws ServiceException {
		
		LOGGER.info("Entra controller Aduana por id {}", id);

		Optional<Turno> turno;
		Respuesta response = new Respuesta();

		if (id == null) {
			LOGGER.warn("Se necesita parametro Id");
			response.setEstatus("error");
			response.setMensaje("Se necesita parametro Id");
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		turno = iTurnoService.consultarTurnoId(id);
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setDto(turno);
		if (!turno.isPresent()) {
			LOGGER.info("Registro no encontrado");
			throw new ServiceException(ErrorEnum.EXC_INEXISTENTE);
		} else {
			response.setValor(turno.get().getFolio().toString());
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK); 
	}
	
	/** Servicio para borrar */
	@ApiOperation(value = "Elimina el registro del turno", notes = "No devuelve ningun valor.")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Respuesta> eliminarTurno(
			@PathVariable("id") Long id) {

		LOGGER.info("Entra a controller, eliminacion de turno id: {}", id);
		Respuesta response = new Respuesta();

		iTurnoService.eliminarTurno(id);
		response.setEstatus("OK");
		response.setMensaje("Borrado exitosa");
		response.setValor(id.toString());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}	

}
