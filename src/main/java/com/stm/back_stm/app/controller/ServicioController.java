package com.stm.back_stm.app.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stm.back_stm.app.domain.entity.Servicio;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.ServicioDTO;
import com.stm.back_stm.app.services.IServicioService;
import com.stm.back_stm.util.Constantes;
import com.stm.back_stm.util.Respuesta;

import io.swagger.annotations.ApiOperation;


/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@RestController
@CrossOrigin
@RequestMapping("/stm/servicios")
public class ServicioController {


	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(ServicioController.class);	
	
	
	@Autowired
	IServicioService iServicioService;
	
	/** Servicio para consultar todo */
	@ApiOperation(value = "Obtiene todos los registros de servicio", notes = "Devuelve un listado de todos los registros de servicio.")
	@GetMapping
	public ResponseEntity<Respuesta> consultaServicio() {
		
		LOGGER.info("Entra a controller para consultar las servicio");
		
		List<Servicio> lista;
		Respuesta response = new Respuesta();		
		
		lista = iServicioService.consultarServicio();
		
		if (lista.isEmpty()) {
			LOGGER.warn("Sin Datos en la base de datos");
			
			response.setEstatus("OK");
			response.setMensaje("Consulta exitosa - sin datos en la base de datos");
			response.setLista(lista);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		LOGGER.warn("Consulta Exitosa");
		
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setLista(lista);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para guardar 
	 * @throws ParseException */
	@ApiOperation(value = "Inserta nuevo registro de servicio", notes = "Devuelve el registro nuevo del servicio insertado.")
    @PostMapping
	public ResponseEntity<Respuesta> guardarServicio(@RequestBody ServicioDTO servicioDTO) throws ServiceException {
		
		LOGGER.info("Entra a controller para guardar las materias");
		LOGGER.info(Constantes.MENSAJE_LOG_PARAMETROS, servicioDTO);
		
		Respuesta response = new Respuesta();

		servicioDTO = iServicioService.guardarServicio(servicioDTO);
		
		response.setEstatus("OK");
		response.setMensaje("Guardado exitoso");	
		response.setValor(servicioDTO.getIdServicio().toString());
		response.setDto(servicioDTO);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/** Servicio para actualizar 
	 * @throws ServiceException */
	@ApiOperation(value = "Actualiza el registro del servicio", notes = "Devuelve el registro del servicio del id especificado.")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Respuesta> actualizarServicio(
			@PathVariable("id") Long id,
	        @Valid @RequestBody ServicioDTO servicioDTO) throws ServiceException {

		LOGGER.info("Entra a controller, actualizacion : {}", servicioDTO);
		Respuesta response = new Respuesta();
		
	    	
		servicioDTO.setIdServicio(id);
		servicioDTO = iServicioService.actualizarServicio(servicioDTO);
		response.setEstatus("OK");
		response.setMensaje("Actualizacion exitoso");
		response.setValor(servicioDTO.getIdServicio().toString());
		response.setDto(servicioDTO);
			
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para consultar por id 
	 * @throws ServiceException */
	@ApiOperation(value = "Buscar servicio por Id", notes = "Devuelve el servicio en base al id especificado.")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Respuesta> consultarServicioPorId(@PathVariable("id") Long id) throws ServiceException {
		
		LOGGER.info("Entra controller Aduana por id {}", id);

		Optional<Servicio> servicio;
		Respuesta response = new Respuesta();

		if (id == null) {
			LOGGER.warn("Se necesita parametro Id");
			response.setEstatus("error");
			response.setMensaje("Se necesita parametro Id");
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		servicio = iServicioService.consultarServicioId(id);
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setDto(servicio);
		if (!servicio.isPresent()) {
			LOGGER.info("Registro no encontrado");
			throw new ServiceException(ErrorEnum.EXC_INEXISTENTE);
		} else {
			response.setValor(servicio.get().getIdServicio().toString());
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK); 
	}
	
	/** Servicio para borrar */
	@ApiOperation(value = "Elimina el registro del servicio", notes = "No devuelve ningun valor.")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Respuesta> eliminarServicio(
			@PathVariable("id") Long id) {

		LOGGER.info("Entra a controller, eliminacion de servicio id: {}", id);
		Respuesta response = new Respuesta();

		iServicioService.eliminarServicio(id);
		response.setEstatus("OK");
		response.setMensaje("Borrado exitosa");
		response.setValor(id.toString());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}	

}
