package com.stm.back_stm.app.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stm.back_stm.app.domain.entity.Empleado;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.EmpleadoDTO;
import com.stm.back_stm.app.services.IEmpleadoService;
import com.stm.back_stm.util.Constantes;
import com.stm.back_stm.util.Respuesta;

import io.swagger.annotations.ApiOperation;


/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@RestController
@CrossOrigin
@RequestMapping("/stm/empleados")
public class EmpleadoController {


	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadoController.class);	
	
	
	@Autowired
	IEmpleadoService iEmpleadoService;
	
	/** Servicio para consultar todo */
	@ApiOperation(value = "Obtiene todos los registros de empleado", notes = "Devuelve un listado de todos los registros de empleado.")
	@GetMapping
	public ResponseEntity<Respuesta> consultaEmpleado() {
		
		LOGGER.info("Entra a controller para consultar las empleado");
		
		List<Empleado> lista;
		Respuesta response = new Respuesta();		
		
		lista = iEmpleadoService.consultarEmpleado();
		
		if (lista.isEmpty()) {
			LOGGER.warn("Sin Datos en la base de datos");
			
			response.setEstatus("OK");
			response.setMensaje("Consulta exitosa - sin datos en la base de datos");
			response.setLista(lista);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		LOGGER.warn("Consulta Exitosa");
		
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setLista(lista);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para guardar 
	 * @throws ParseException */
	@ApiOperation(value = "Inserta nuevo registro de empleado", notes = "Devuelve el registro nuevo del empleado insertado.")
    @PostMapping
	public ResponseEntity<Respuesta> guardarEmpleado(@RequestBody EmpleadoDTO empleadoDTO) throws ServiceException {
		
		LOGGER.info("Entra a controller para guardar las materias");
		LOGGER.info(Constantes.MENSAJE_LOG_PARAMETROS, empleadoDTO);
		
		Respuesta response = new Respuesta();

		empleadoDTO = iEmpleadoService.guardarEmpleado(empleadoDTO);
		
		response.setEstatus("OK");
		response.setMensaje("Guardado exitoso");	
		response.setValor(empleadoDTO.getIdEmpleado().toString());
		response.setDto(empleadoDTO);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/** Servicio para actualizar 
	 * @throws ServiceException */
	@ApiOperation(value = "Actualiza el registro del empleado", notes = "Devuelve el registro del empleado del id especificado.")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Respuesta> actualizarEmpleado(
			@PathVariable("id") Long id,
	        @Valid @RequestBody EmpleadoDTO empleadoDTO) throws ServiceException {

		LOGGER.info("Entra a controller, actualizacion : {}", empleadoDTO);
		Respuesta response = new Respuesta();
		
	    	
		empleadoDTO.setIdEmpleado(id);
		empleadoDTO = iEmpleadoService.actualizarEmpleado(empleadoDTO);
		response.setEstatus("OK");
		response.setMensaje("Actualizacion exitoso");
		response.setValor(empleadoDTO.getIdEmpleado().toString());
		response.setDto(empleadoDTO);
			
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para consultar por id 
	 * @throws ServiceException */
	@ApiOperation(value = "Buscar empleado por Id", notes = "Devuelve el empleado en base al id especificado.")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Respuesta> consultarEmpleadoPorId(@PathVariable("id") Long id) throws ServiceException {
		
		LOGGER.info("Entra controller Aduana por id {}", id);

		Optional<Empleado> empleado;
		Respuesta response = new Respuesta();

		if (id == null) {
			LOGGER.warn("Se necesita parametro Id");
			response.setEstatus("error");
			response.setMensaje("Se necesita parametro Id");
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		empleado = iEmpleadoService.consultarEmpleadoId(id);
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setDto(empleado);
		if (!empleado.isPresent()) {
			LOGGER.info("Registro no encontrado");
			throw new ServiceException(ErrorEnum.EXC_INEXISTENTE);
		} else {
			response.setValor(empleado.get().getIdEmpleado().toString());
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK); 
	}
	
	/** Servicio para borrar */
	@ApiOperation(value = "Elimina el registro del empleado", notes = "No devuelve ningun valor.")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Respuesta> eliminarEmpleado(
			@PathVariable("id") Long id) {

		LOGGER.info("Entra a controller, eliminacion de empleado id: {}", id);
		Respuesta response = new Respuesta();

		iEmpleadoService.eliminarEmpleado(id);
		response.setEstatus("OK");
		response.setMensaje("Borrado exitosa");
		response.setValor(id.toString());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}	

}
