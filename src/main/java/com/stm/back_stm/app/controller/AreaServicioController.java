package com.stm.back_stm.app.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stm.back_stm.app.domain.entity.AreaServicio;
import com.stm.back_stm.app.exception.ErrorEnum;
import com.stm.back_stm.app.exception.ServiceException;
import com.stm.back_stm.app.model.AreaServicioDTO;
import com.stm.back_stm.app.services.IAreaServicioService;
import com.stm.back_stm.util.Constantes;
import com.stm.back_stm.util.Respuesta;

import io.swagger.annotations.ApiOperation;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

@RestController
@CrossOrigin
@RequestMapping("/stm/areas-servicios")
public class AreaServicioController {
	
	/** La Constante logger. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(AreaServicioController.class);	
	
	
	@Autowired
	IAreaServicioService iAreaServicioService;
	
	/** Servicio para consultar todo */
	@ApiOperation(value = "Obtiene todos los registros de areaServicio", notes = "Devuelve un listado de todos los registros de areaServicio.")
	@GetMapping
	public ResponseEntity<Respuesta> consultaAreaServicio() {
		
		LOGGER.info("Entra a controller para consultar las areaServicio");
		
		List<AreaServicio> lista;
		Respuesta response = new Respuesta();		
		
		lista = iAreaServicioService.consultarAreaServicio();
		
		if (lista.isEmpty()) {
			LOGGER.warn("Sin Datos en la base de datos");
			
			response.setEstatus("OK");
			response.setMensaje("Consulta exitosa - sin datos en la base de datos");
			response.setLista(lista);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		LOGGER.warn("Consulta Exitosa");
		
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setLista(lista);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para guardar 
	 * @throws ParseException */
	@ApiOperation(value = "Inserta nuevo registro de areaServicio", notes = "Devuelve el registro nuevo del areaServicio insertado.")
    @PostMapping
	public ResponseEntity<Respuesta> guardarAreaServicio(@RequestBody AreaServicioDTO areaServicioDTO) throws ServiceException {
		
		LOGGER.info("Entra a controller para guardar las materias");
		LOGGER.info(Constantes.MENSAJE_LOG_PARAMETROS, areaServicioDTO);
		
		Respuesta response = new Respuesta();

		areaServicioDTO = iAreaServicioService.guardarAreaServicio(areaServicioDTO);
		
		response.setEstatus("OK");
		response.setMensaje("Guardado exitoso");	
		response.setValor(areaServicioDTO.getIdArea().toString());
		response.setDto(areaServicioDTO);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/** Servicio para actualizar 
	 * @throws ServiceException */
	@ApiOperation(value = "Actualiza el registro del areaServicio", notes = "Devuelve el registro del areaServicio del id especificado.")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Respuesta> actualizarAreaServicio(
			@PathVariable("id") Long id,
	        @Valid @RequestBody AreaServicioDTO areaServicioDTO) throws ServiceException {

		LOGGER.info("Entra a controller, actualizacion : {}", areaServicioDTO);
		Respuesta response = new Respuesta();
		
	    	
		areaServicioDTO.setIdArea(id);
		areaServicioDTO = iAreaServicioService.actualizarAreaServicio(areaServicioDTO);
		response.setEstatus("OK");
		response.setMensaje("Actualizacion exitoso");
		response.setValor(areaServicioDTO.getIdArea().toString());
		response.setDto(areaServicioDTO);
			
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/** Servicio para consultar por id 
	 * @throws ServiceException */
	@ApiOperation(value = "Buscar areaServicio por Id", notes = "Devuelve el areaServicio en base al id especificado.")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Respuesta> consultarAreaServicioPorId(@PathVariable("id") Long id) throws ServiceException {
		
		LOGGER.info("Entra controller Aduana por id {}", id);

		Optional<AreaServicio> areaServicio;
		Respuesta response = new Respuesta();

		if (id == null) {
			LOGGER.warn("Se necesita parametro Id");
			response.setEstatus("error");
			response.setMensaje("Se necesita parametro Id");
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		areaServicio = iAreaServicioService.consultarAreaServicioId(id);
		response.setEstatus("OK");
		response.setMensaje("Consulta exitosa");
		response.setDto(areaServicio);
		if (!areaServicio.isPresent()) {
			LOGGER.info("Registro no encontrado");
			throw new ServiceException(ErrorEnum.EXC_INEXISTENTE);
		} else {
			response.setValor(areaServicio.get().getIdArea().toString());
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK); 
	}
	
	/** Servicio para borrar */
	@ApiOperation(value = "Elimina el registro del areaServicio", notes = "No devuelve ningun valor.")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Respuesta> eliminarAreaServicio(
			@PathVariable("id") Long id) {

		LOGGER.info("Entra a controller, eliminacion de areaServicio id: {}", id);
		Respuesta response = new Respuesta();

		iAreaServicioService.eliminarAreaServicio(id);
		response.setEstatus("OK");
		response.setMensaje("Borrado exitosa");
		response.setValor(id.toString());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}	

}
