package com.stm.back_stm.app.exception;

import com.stm.back_stm.util.Constantes;


/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

/** 
 * The Enum ErrorEnum.
 * 
 * Esta clase permite la enumeracion de diferentes mensajes de excepcion
 * utilizados en los cuerpos de respuesta HTTP arrojados por el servicio
 * 
 * Es posible agregar nuevos mensajes personalizados
 * para permitir que el servicio sea mas explicito,
 * recordando siempre que es importante evitar arrojar informacion sensible
 * 
 */
public enum ErrorEnum {
	
	EXC_GENERICO("EXC.000", "Error generico", "Error generico de servidor", Constantes.ERROR, ""),

	EXC_ERROR_PARAMS("EXC.001", "Parametros invalidos", "Parametros invalidos de consumo", Constantes.WARNING, ""),
	
	EXC_DUPLICADO("EXC.100", "Duplicado", "Ya existe, no puede ser sobrescrita", Constantes.WARNING, ""),
	
	EXC_INEXISTENTE("EXC.101", "Registro Inexistente", "No existe, intente con otro valor", Constantes.WARNING, ""),
	
	EXC_OPER_NO_EXITOSA("EXC.102", "Operacion no exitosa", "Operacion no exitosa", Constantes.ERROR, ""),

	EXC_OPER_CON_ERRORES("EXC.103", "Operacion con errores", "Operacion con errores", Constantes.ERROR, ""),
	
	EXC_TOKEN_NO_DISPONIBLE("EXC.104", "Servicio token no disponible", "Servicio token no disponible", Constantes.ERROR, ""),

	EXC_SERVICIO_NO_DISPONIBLE("EXC.105", "Servicio no disponible", "Servicio no disponible", Constantes.ERROR, "");


	/** codigo de la operacion */

    private final String code;
	/** propiedad que muestra   mensaje de error   */ 
    private final String message;
    /** propiedad que muestra  la descripcion  del mensaje de error  */  
	private final String description;
   
	/** propiedad que muestra nivel  */ 
     private final String level;
    
 	/** propiedad que muestra informacion mas adetalle del error  */  
    private final String moreInfo;

 	/** constructor de la clase ErrorEnum  que contiene parametros separados de los  diferentes mensajes de excepcion */      
    ErrorEnum(final String code, final String message, 
    		final String description, final String level, final String moreInfo ) {
        this.code = code;
        this.message = message;
        this.description = description;
        this.level = level;
        this.moreInfo = moreInfo;
    }

    /**
     *  get que retorna  el codigo 
     */
	public String getCode() {
		return code;
	}

	 /**
     *  get que retorna  el  mensaje
     */
	public String getMessage() {
		return message;
	}
    
	 /**
     *  get que retorna  la descripcion 
     */
	public String getDescription() {
		return description;
	}

	 /**
     *  get que retorna  el nivel  
     */
	public String getLevel() {
		return level;
	}

	 /**
     *  get que retorna  el mensage de informacion
     */
	public String getMoreInfo() {
		return moreInfo;
	}

}
