package com.stm.back_stm.app.exception;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

public class ServiceException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public ServiceException(String message) {
        super(message);
    }

    public ServiceException(final String message, final Throwable e) {
        super(message, e);
    }
    
    public ServiceException(ErrorEnum errorEnum) {
        super(errorEnum.getMessage());
    }
    

}