package com.stm.back_stm.util;

/**
 * @author J.Moya
 * @author D.Aguilar
 *
 */

public class Constantes {

	public static final String MENSAJE_LOG_PARAMETROS = "Entra a controller, creacion : {}";
	/** propiedad de  informacion  */
	public static final String INFO = "Info";
	/** propiedad  de error */
	public static final String ERROR = "Error";
	/** propiedad  de emergencia  */
	public static final String WARNING = "Warning";
	/** propiedad  llave  que  se le asigna al redis para optener el token  */
	public static final String TOKEN_KEY = "accessTokenKey1";
	/** propiedad  de respuesta   */
    public static final int MILI_SEGUNDOS = 1000;
	/** propiedad   key_scope  de obtenTokenAcceso   */
	public static final String KEY_SCOPE = "scope";
	/** propiedad   KEY_GRANT_TYPE de obtenTokenAcceso  */
	public static final String KEY_GRANT_TYPE = "grant_type";
	/** propiedad  KEY_CLIENT_ID  de StatusBiometrico   */
	public static final String KEY_CLIENT_ID = "client_id";
	/** propiedad  KEY_CLIENT_SECRET  obtenTokenAcceso  */  
	public static final String KEY_CLIENT_SECRET = "client_secret";
	/** propiedad VALUE_SCOPE  de solicitaTokenAcceso   */  
	public static final String VALUE_GRANT_TYPE = "client_credentials";
	/** propiedad HEADER_KEY_CONTENT_TYPE para el status   */  
	public static final String HEADER_KEY_CONTENT_TYPE = "Content-Type";
	/** propiedad HEADER_KEY_ACCEPT para el status   */ 
	public static final String HEADER_KEY_ACCEPT = "accept";
	/** propiedad HEADER_KEY_XIBM_CLIENT_ID para el status   */ 
	public static final String HEADER_KEY_XIBM_CLIENT_ID = "x-ibm-client-id";
	/** propiedad HEADER_KEY_AUTHORIZATION para el status   */ 
	public static final String HEADER_KEY_AUTHORIZATION = "Authorization";
	/** propiedad CLIENT_ID  para el   obtenTokenAcces  */ 
    public static final String CLIENT_ID = "CLIENT_ID";
	/** propiedad TOKEN_ACCESS  para el   obtenTokenAcces  */ 
	public static final String TOKEN_ACCESS = "TOKEN_ACCESS";
	/** propiedad REQUEST_BODY para el  body status  */ 
	public static final String REQUEST_BODY = "REQUEST_BODY";
	/** propiedad CONTEXTO_SEGURO  contesto de seguridad local    */ 
	public static final String CONTEXTO_SEGURO = "SSL";
	/** propiedad AMBIENTE  para identificar un ambiente local de desarrollo     */ 
	public static final String AMBIENTE = "AMBIENTE";
	/** propiedad AMBIENTE  para identificar un ambiente local de desarrollo     */ 
	public static final String AMBIENTE_LOCAL = "DEV";
	/** propiedad HEADER_VALUE_CONTENT_TYPE  para el status  */ 
	public static final String HEADER_VALUE_CONTENT_TYPE = "application/json";
	/** propiedad HEADER_VALUE_ACCEPT  para el status  */ 
	public static final String HEADER_VALUE_ACCEPT = "application/json";
	/** propiedad  HEADER_VALUE_XIBM_CLIENT_ID   para el status  */ 
	public static final String HEADER_VALUE_XIBM_CLIENT_ID = CLIENT_ID;
	/** propiedad HEADER_VALUE_AUTHORIZATION para el status  */ 
	public static final String HEADER_VALUE_AUTHORIZATION = "Bearer " + TOKEN_ACCESS;
	
	/** */
	public static final String FECHA_HORA_FORMATO_AMD = "yyyy-MM-dd HH:mm:ss";
	
	public static final String FORMATO_FECHA = "dd-MM-yyyy";
	public static final String FORMATO_HORA = "HH:mm:ss";
	

	public static final String HORA_INI = "00:00:00";
	public static final String HORA_FIN = "23:59:59";
	
	/** propiedad status de las facturas */
	public static final int ESTATUS_PENDIENTE = 1;
	public static final int ESTATUS_CANCELAR = 3;
	

	//Agregar nuevas constantes aqui
  
	/**
	 * Constructor de la clase  Constantes 
	 */
	private Constantes() {} // previene instanciacion

	/**
	 * Sirve para extraer CRLF (Retorno de carro y salto de linea)
 	 *
	 * @param  pDato String en donde se busca CRLF y se extrae.
	 * @return String regresa el valor del string sin CRLF
	 */
	public static String extractCRLF(String pDato) {
		return pDato.replace("\r", "").replace("\n", "");
	}
}